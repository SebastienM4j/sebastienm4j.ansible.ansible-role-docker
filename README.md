Ansible Role Docker
===================

Installs Docker Community Edition.


Usage
-----

### Role Variables

Available variables are listed below, along with default values (see [`defaults/main.yml`](defaults/main.yml)).

#### `docker_service_enabled: true`

`true` if the Docker service starts at boot.

#### `docker_users: []`

List of users to add in the `docker` group.

#### `docker_config: {}`

Configuration of the Docker daemon.

This configuration is copied in `/etc/docker/daemon.json`.

### Example Playbook

```yaml
- hosts: server
  roles:
    
    - role: sebastienm4j.docker
      docker_service_enabled: false
      docker_users:
        - ubuntu
      docker_config: { "iptables": false }
```


Credits
-------

Inspired by https://github.com/geerlingguy/ansible-role-docker and https://github.com/avinetworks/ansible-role-docker.


License
-------

Ansible Role Iptables is release under [MIT licence](LICENSE).
